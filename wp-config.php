<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'starter');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vqZjdkjCD+>pZo{%$&aw#FlVkHAm?Jj+ q}QiMT-Qak{v^-aq9_T|cB!edmx@>]q');
define('SECURE_AUTH_KEY',  'I!OS2[G=fw7hAq,rdW!C7)(VmAdNL[|%0r|AJ;W_W%ev_kx?5TG~9?O%H9)IA.=f');
define('LOGGED_IN_KEY',    'p%]?f&{oL*7ckEPDR8~U=rfX9Q@x]td}_@G,NLFKDT(U4a)b43+5p=txlxwu)F4B');
define('NONCE_KEY',        '1fAePN<F8``txOkhRQ6P_tU;8(TFE[]Hkg{f5zKs?- a:=xuI;{V$r8QI:Kp#/FJ');
define('AUTH_SALT',        'R^-q*J)Kw8SH!s9.zj:N_Gtf3-m[yWM<`6|X23r<1mE_C*-FX]k| }3Uxm5GZ]K6');
define('SECURE_AUTH_SALT', ')sJdt`iI7PwKnh,G[HCrK5e}p*hUzRh^|Izm;#-i(|l;r&A4$/1rZ!@g5=< n60/');
define('LOGGED_IN_SALT',   '<8bn)Q wdQsPb#W[f+;lQWFHTiZLxYL)GsI7}r::~aR%n$eRQ|#?h5([.?66[8;[');
define('NONCE_SALT',       'qrB3MefjWRHH[K(GAj&f%Y][2t4 ~G85jQ,5g9)0.R+^#=;R[cP~-S4,@{W%rP@0');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'dry_';

/**
 * O idioma localizado do WordPress é o inglês por padrão.
 *
 * Altere esta definição para localizar o WordPress. Um arquivo MO correspondente ao
 * idioma escolhido deve ser instalado em wp-content/languages. Por exemplo, instale
 * pt_BR.mo em wp-content/languages e altere WPLANG para 'pt_BR' para habilitar o suporte
 * ao português do Brasil.
 */
define('WPLANG', 'pt_BR');

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
