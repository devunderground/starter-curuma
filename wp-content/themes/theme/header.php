<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="utf-8" />

	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta name="author" content="" />
	<link type="text/plain" rel="author" href="<?php echo get_template_directory_uri(); ?>/humans.txt" />

	 <meta name="viewport" content="width=device-width, initial-scale=1" /> 
	
	<!-- Google Chrome Frame for IE -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="google-site-verification" content="" />

	<link rel="icon" href="<?php echo WP_IMAGE_URL ?>/icons/favicon.png" />
	<link rel="apple-touch-icon" href="<?php echo WP_IMAGE_URL ?>/icons/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="57x57"   href="<?php echo WP_IMAGE_URL ?>/icons/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" sizes="72x72"   href="<?php echo WP_IMAGE_URL ?>/icons/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo WP_IMAGE_URL ?>/icons/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo WP_IMAGE_URL ?>/icons/apple-touch-icon-144x144.png" />

	<link rel="stylesheet" href="<?php echo WP_STYLE_URL ?>" />

	<!--[if lt IE 9]>
		<script src="<?php echo WP_SCRIPT_URL ?>/vendor/html5shiv.js"></script>
	<![endif]-->

	<!-- wp_head(); -->
	<?php wp_head(); ?>
	<!-- /wp_head(); -->
</head>
<body <?php body_class(); ?>>
	<!--[if lt IE 9]>
		<p class="chromeframe">
			Você está usando um navegador <strong>desatualizado</strong>.
			Por favor <a href="http://browsehappy.com/">atualize seu navegador</a> ou
			<a href="http://www.google.com/chromeframe/?redirect=true">ative o Google Chrome Frame</a>
			para melhorar sua experiência de navegação.
		</p>
	<![endif]-->
	
	<header class="header cem cf">
		<div class="grid tp cf">
		    <div class="ts">		        
			<a href="<?php echo home_url(); ?>" class="logo img-rep" title="<?php bloginfo( 'name' ); ?>"><h1><?php bloginfo( 'name' ); ?></h1></a>
		    </div>
		</div>
       
        <!-- menu -->
        <div class="cem b-menu"> 
            <div class="grid cf">

            <strong class="dn">MENU PRINCIPAL</strong>              
            <a href="#menu" class="anima-menu">Menu</a>
            <nav id="menu" role="navigation">
               <ul class="menu">
					<li><a href="<?php echo home_url() ?>" class="menu-link" title="Home">Home</a></li>
                    <li><a href="<?php echo get_permalink( get_page_by_path('') ); ?>" class="menu-link" title="">sobre</a></li>               
				</ul>
               
            </nav>

            </div>                  
        </div>
	</header>
		
		<hr class="dn" />
		