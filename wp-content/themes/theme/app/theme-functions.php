<?php

/*
|-----------------------------------------
| Back-end
|-----------------------------------------
*/



/**
 * Customiza o logo no login
 */
function custom_logo_login() {
	echo '
		<style  type="text/css">
			.login h1 a {
				width: 274px;
				height: 124px;
				background-image: url(' . WP_IMAGE_URL . '/login.png);
				background-size:100%;
			}
		</style>
	';
}

/**
 * Customiza a URL da logo no login
 */
function custom_logo_login_url() {
	return home_url();
}

/**
 * Customiza o titulo da logo no login
 */
function custom_logo_login_title() {
    return get_bloginfo( 'name' );
}

/**
 * Customiza o rodapé no admin
 */
function custom_admin_footer() {
	echo '<a target="_blank" href=' . WP_THEME_URL . '>'. custom_logo_login_title() .'</a> &copy; ' . date( 'Y' );
}

/**
 * Esconde a versão do WordPress no admin
 */
function hide_footer_version() {
	return '';
}

/**
 * Remove o logo do WordPress da barra de topo
 */
function remove_logo_toolbar( $wp_toolbar ) {
	global $wp_admin_bar;
	$wp_toolbar->remove_node( 'wp-logo' );
}

/**
 * Esconde links desnecessários do menu lateral no admin
 */
function hide_admin_menu_links() {
	// remove_menu_page( 'tools.php' );
	// remove_submenu_page( 'themes.php', 'theme-editor.php' );
}

/**
 * Remove widgets do dashboard
 */
function remove_dashboard_widgets() {
	global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
}

/*
|-----------------------------------------
| Front-end
|-----------------------------------------
*/

/**
 * Registra e coloca os scripts na fila para execução
 */
function custom_scripts() {
	if( !is_admin() ) {
		wp_deregister_script( 'jquery' );

		wp_register_script( 'jquery', WP_SCRIPT_URL . '/vendor/lib/jquery.min.js', array(), null, true );
		wp_register_script( 'app', WP_SCRIPT_URL . '/main.min.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'app' );
	}
}

/**
 * Otimização para o título das páginas
 * http://bavotasan.com/2012/filtering-wp_title-for-better-seo/
 */
function filter_wp_title( $title ) {
	global $page, $paged;

	if ( is_feed() ) {
		return $title;
	}

	$site_description = get_bloginfo( 'description' );

	$filtered_title  = $title . get_bloginfo( 'name' );
	$filtered_title .= ( ! empty( $site_description ) && ( is_home() || is_front_page() ) ) ? ' | ' . $site_description: '';
	$filtered_title .= ( 2 <= $paged || 2 <= $page ) ? ' | ' . sprintf( __( 'Página %s' ), max( $paged, $page ) ) : '';

	return $filtered_title;
}

/*
|-----------------------------------------
| DEV Underground
|-----------------------------------------
*/


/*
filtro e function para imagem na lista de posts
*/
add_filter('manage_posts_columns', 'posts_columns', 5);
add_action('manage_posts_custom_column', 'posts_custom_columns', 5, 2);

function posts_columns($defaults){
    $defaults['riv_post_thumbs'] = __('Thumbs');
    return $defaults;
}

function posts_custom_columns($column_name, $id){
        if($column_name === 'riv_post_thumbs'){
        echo the_post_thumbnail( 'thumbnail' );
    }
}

/**
 * Remove bar do wp
 */
add_filter('show_admin_bar', '__return_false');

/* menu */
register_nav_menus( array(
	'menu' => __( 'Menu Principal' ),
	'menu-footer' => __( 'Menu Rodapé' )
));

/**
 * Limitar número caracteres conteúdo
 */
function limit( $content, $limit ) {
	if( strlen( $content ) > $limit ) {
		$content = substr( $content, 0, $limit );
		return $content."...";
	} else {
		return $content;
	}
}


/**
 * Limitar número caracteres conteúdo, MAS NÃO CORTA A PALAVRA AO MEIO
 */
function limitarTexto($content, $limite){
  $contador = strlen($content);
  if ( $contador >= $limite ) {
      $content = substr($content, 0, strrpos(substr($content, 0, $limite), ' ')) . '...';
      return $content;
  }
  else{
    return $content;
  }
}


// Tradutor
add_filter( 'gettext', 'change' ); 
add_filter( 'ngettext', 'change' ); 

function change( $translated_text ) { 
    switch ( $translated_text ) { 
        case 'Posts' : $translated_text = __( 'Notícias'); 
        break; 
    } 
    return 
    $translated_text; 
}




/**
 * Insere o slug da page como class no body
 */
function add_body_class( $classes )
{
    global $post;
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}
add_filter( 'body_class', 'add_body_class' );

?>
